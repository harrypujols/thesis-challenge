const FRAMEWORK = FRAMEWORK || {}

import components from './methods/components'
import fields from './components/fields'
import visibility from './components/visibility'

import run from './app/run'

(( window, APP ) => {

  APP.methods = {
    components
  }

  APP.components = {
    fields,
    visibility
  }

  APP.start = {
    run
  }

  APP.start.run( APP )

})( window, FRAMEWORK, undefined )
