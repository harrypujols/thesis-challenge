export default class {
  constructor ( element ) {
    this.element = element
    this.fields =  this.element.querySelectorAll('.js-field')
  }

  init ( ) {
    this.fields.forEach(( field ) => {
      let label = field.parentElement.querySelector('.js-field-label')
      field.addEventListener('blur', ( event ) => {
        if (field.value != "") {
          label.classList.add('is-hidden')
        } else {
          label.classList.remove('is-hidden')
        }
      })
    })
  }
}
