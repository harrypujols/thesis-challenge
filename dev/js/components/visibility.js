export default class {
  constructor ( element ) {
    this.element = element
    this.field = this.element.querySelector('.js-visibility-field')
    this.button = this.element.querySelector('.js-visibility-field-button')
  }

  init ( ) {
    this.button.addEventListener('click', ( event ) => {
        if (this.field.type == 'password') {
          this.field.type = 'text'
          this.button.classList.add('is-visible')
        } else {
          this.field.type = 'password'
          this.button.classList.remove('is-visible')
        };
    })
  }
}
